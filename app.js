new Vue({
    el: '#app',
    data: {
        playerHealth: 100,
        monsterHealth: 100,
        gameIsRunning: false,
        turns: []
    },
    methods: {
        startGame: function () {
            this.gameIsRunning = true;
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.turns = [];
        },
        attack: function () {
            // player attacks
            let damageByPlayer = this.calculateDamage(3, 10);
            this.monsterHealth -= damageByPlayer;
            this.turns.unshift({
                isPlayer: true,
                text: 'Player hits Monster for ' + damageByPlayer
            });

            if (this.checkWin()) {
                return;
            }
            //monster attacks
            this.monsterAttacks();
            this.checkWin();
        },
        specialAttack: function () {
            let damageByPlayer = this.calculateDamage(10, 20);
            this.monsterHealth -= damageByPlayer;
            this.turns.unshift({
                isPlayer: true,
                text: 'Player hits Monster hard for ' + damageByPlayer
            });
            if (this.checkWin()) {
                return;
            }
            this.monsterAttacks();
            this.checkWin();
        },
        heal: function () {
            if (this.playerHealth <= 90) {
                this.playerHealth += 10;
            } else {
                this.playerHealth = 100;
            }

            this.turns.unshift({
                isPlayer: true,
                text: 'Player heals for 10'
            });

            this.monsterAttacks();
        },
        giveUp: function () {
            this.gameIsRunning = false;
        },
        monsterAttacks: function () {
            let damageByMonster = this.calculateDamage(5, 12);
            this.playerHealth -= damageByMonster;

            this.turns.unshift({
                isPlayer: false,
                text: 'Monster hits Player for ' + damageByMonster
            });
        },
        calculateDamage: function (minDmg, maxDmg) {
            return Math.max(Math.floor(Math.random() * maxDmg) + 1, minDmg)
        },
        checkWin: function () {
            if (this.monsterHealth <= 0) {
                if (confirm('You won! New Game?')) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false;
                }
                return true;
            } else if (this.playerHealth <= 0) {
                if (confirm('You lost! New Game?')) {
                    this.startGame();
                } else {
                    this.gameIsRunning = false;
                }
                return true;
            }
            return false;
        }
    }
});